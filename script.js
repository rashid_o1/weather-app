function getWeather() {
  const apiKey = 'f4dc1bd38a5ff3c894f5287f6cdf980b';
  const cityInput = document.getElementById('city-input');
  const locationElement = document.getElementById('location');
  const temperatureElement = document.getElementById('temperature');
  const descriptionElement = document.getElementById('description');

  const city = cityInput.value;

  if (city === '') {
    locationElement.textContent = 'Please enter a city';
    temperatureElement.textContent = '';
    descriptionElement.textContent = '';
    return;
  }

  // Make a request to OpenWeatherMap API
  fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`)
    .then(response => {
      if (!response.ok) {
        throw new Error('Unable to fetch weather data');
      }
      return response.json();
    })
    .then(data => {
      // Extract relevant data from the API response
      const location = data.name;
      const temperature = data.main.temp;
      const description = data.weather[0].description;

      // Update the HTML elements with the fetched data
      locationElement.textContent = `Location: ${location}`;
      temperatureElement.textContent = `Temperature: ${temperature}°C`;
      descriptionElement.textContent = `Description: ${description}`;
    })
    .catch(error => {
      console.log('Error fetching weather data:', error);
      locationElement.textContent = 'Unable to fetch weather data';
      temperatureElement.textContent = '';
      descriptionElement.textContent = '';
    });
}
